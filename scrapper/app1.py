"""
App 1:
Scrieți un script care deschide un URL specificat de la tastatură și printează titlul paginii HTML și description meta
"""

import bs4 as bs
import requests

str=""
# str='https://pythonprogramming.net/parsememcparseface/'
while(not(str and str.strip())):
    str=input("Introduceti un site: ")

source= requests.get(str).text

soup= bs.BeautifulSoup(source, 'html.parser') #cu lxml nu gasesc conentul de la metadata
print(f'Titlul paginii: {soup.title.text}')

desc_metadata= soup.find('meta', attrs={'name': 'description'})
print(desc_metadata.attrs['content'])


